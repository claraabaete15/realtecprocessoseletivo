#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

typedef struct sShow{
    char nomeArtista[30];
    char genero[30];
    float valorPagar;
    float horaInicio, horaFim;
}Show;
typedef struct sCelula{
    struct sCelula* ant;
    struct sCelula* prox;
    Show info;
}Celula;
typedef struct sDia{
    struct sCelula* principal;
    struct sCelula* inicial;
}Dia;
Celula* criar_celula(){
		Celula *nova;
		nova=(Celula*)malloc(sizeof(Celula));
		if(nova!=NULL){
			nova->ant=NULL;
			nova->prox=NULL;
		}
			return nova;
}

int dia_vazio(Celula *lista){
	
	if(lista==NULL){
		return 1;
	}
	return 0;
}
Show preencherShow(){
    Show info;
    int hora;
    fflush(stdin);
    printf("Informe o nome do artista:\n");
    fflush(stdin);
    gets(info.nomeArtista);
    printf("Informe o genero musical:\n");
    fflush(stdin);
    gets(info.genero);
	do{
	    printf("Informe o valor a pagar:\n");
	    scanf("%f",&info.valorPagar);
	}while(info.valorPagar<0);
	do{
	    printf("Informe a hora de inicio:\n");
		scanf("%f",&info.horaInicio);
	}while(info.horaInicio<0||info.horaInicio>24);
	do{
	    printf("Informe a hora termino:\n");
    	scanf("%f",&info.horaFim);
	}while(info.horaFim<0||info.horaFim>24);
    return info;
}

int cadastrarShow(Dia* dias, int dia, int principal){
		Celula *nova = criar_celula();
		Celula*aux = dias[dia].inicial;
		nova->info = preencherShow();
		
		if(principal==1)
            dias[dia].principal = nova;
		if(dia_vazio(dias[dia].inicial)==1){
			dias[dia].principal = nova;
            dias[dia].inicial = nova;
			printf("Inserido com sucesso!\n");
			return 1;
	    }
	    do{
            if(aux->info.horaInicio > nova->info.horaInicio){
                if(aux->ant==NULL)
                    dias[dia].inicial = nova;
                nova->ant=aux->ant;
                nova->prox=aux;
                aux->ant=nova;
                printf("Inserido com sucesso!\n");
                return 1;
            }
            if(aux->prox==NULL){
                nova->ant=aux;
                aux->prox=nova;
                printf("Inserido com sucesso!\n");
                return 1;
            }
            aux=aux->prox;
        }while(aux!=NULL);
        return 0;
}
void freeAll(Dia* dias){
    int i;
    Celula *aux;
    for(i=0;i<5;i++){
        while(dias[i].inicial!=NULL){
            aux=dias[i].inicial;
            dias[i].inicial=dias[i].inicial->prox;
            free(aux);
        }
    }
}

void iniciarDias(Dia* dias){
	int i=0;
	for(i=0;i < 5;i ++){
		dias[i].inicial=NULL;
		dias[i].principal=NULL;
	}
}
void exibirProgramacao(Dia* dias){
    int i,f;
    Celula *aux;
    for(i=0;i<5;i++){
    	if(dia_vazio(dias[i].inicial)){
    		printf("\nDia %d Nada Programado!",i+1);
    		continue;
		}
        aux = dias[i].principal;
        f=i+1;
        printf("\nDia: %d\nAtracao Principal:",f);
        printf("\nArtista: %s", aux->info.nomeArtista,"\nGenero: %s", aux->info.genero,"\nValor a pagar: %.2f",aux->info.valorPagar);
        printf("\nHora de inicio: %.2f\nHora de fim: %.2f",aux->info.horaInicio,aux->info.horaFim);
        aux = dias[i].inicial;
        printf("\n\nProgramacao:");
        while(aux!=NULL){
            printf("\nArtista: %s\nGenero: %s\nValor a pagar: %.2f", aux->info.nomeArtista, aux->info.genero,aux->info.valorPagar);
            printf("\nHora de inicio: %.2f\nHora de fim: %.2f\n",aux->info.horaInicio,aux->info.horaFim);
            aux=aux->prox;
        }
    }
    printf("\n");

}
void salvarProgramacao(Dia* dias){
    int i,f;
    Celula *aux;
    FILE *arq ;
    char nome[30]={""};
    char nomeA[40],diaChar[10];
	fflush(stdin);
    printf("\nNome do arquivo:\n");
    gets(nome);
    for(i=0;i<5;i++){
    	itoa(i+1,diaChar,10);
    	strcpy(nomeA,nome);
    	strcat(nomeA, " Dia ");
    	strcat(nomeA, diaChar);
    	strcat(nomeA,".txt");
    	arq = fopen( nomeA , "w" );
    	if(arq==NULL){	
			printf("\nNao foi possivel salvar o arquivo %s",nomeA);
			continue;
		}
    	if(dia_vazio(dias[i].inicial)){
    		fprintf(arq ,"\nDia %d Nada Programado!",i+1);
	        fclose(arq);
	        printf("\nArquivo %s Salvo Com Sucesso\n",nomeA);
    		continue;
		}
        aux = dias[i].principal;
        f=i+1;
        fprintf(arq ,"\nDia: %d\nAtracao Principal:",f);
        fprintf(arq ,"\nArtista: %s", aux->info.nomeArtista,"\nGenero: %s", aux->info.genero,"\nValor a pagar: %.2f",aux->info.valorPagar);
        fprintf(arq ,"\nHora de inicio: %.2f\nHora de fim: %.2f",aux->info.horaInicio,aux->info.horaFim);
        aux = dias[i].inicial;
        fprintf(arq ,"\n\nProgramacao:");
        while(aux!=NULL){
            fprintf(arq ,"\nArtista: %s\nGenero: %s\nValor a pagar: %.2f", aux->info.nomeArtista, aux->info.genero,aux->info.valorPagar);
            fprintf(arq ,"\nHora de inicio: %.2f\nHora de fim: %.2f\n",aux->info.horaInicio,aux->info.horaFim);
            aux=aux->prox;
        }
        fclose(arq);
        printf("\nArquivo %s Salvo Com Sucesso\n",nomeA);
    }

}

int removerShow(Dia*dias, int dia,char artista[30]){
    int op=0;
    Celula *aux;
	aux=dias[dia].inicial;
    while(aux!=NULL){
        if(strcmp(aux->info.nomeArtista,artista)==0){
        	do{
	            printf("\nDeseja remover a atracao: \nArtista: %s\nGenero: %s\nValor a pagar: %.2f", aux->info.nomeArtista, aux->info.genero,aux->info.valorPagar);
	            printf("\nHora de inicio: %.2f\nHora de fim: %.2f\n[0]Nao.\n[1]Sim.",aux->info.horaInicio,aux->info.horaFim);
	            scanf("%d",&op);
			}while(op<0||op>1);
            if (op==1){
                if (aux->ant!=NULL)
                    aux->ant->prox=aux->prox;
                else
                	dias[dia].inicial=aux->prox;
                if (aux->prox!=NULL)
                    aux->prox->ant=aux->ant;
                if(aux==dias[dia].principal){
                	if(aux->prox!=NULL){
                		dias[dia].principal = aux->prox;
					}else if(aux->ant!=NULL){
						dias[dia].principal = aux->ant;
					}else{
						dias[dia].principal=NULL;
					}
					
				}
                free(aux);
                return 0;
            }else{
                return 1;
            }
        }
        aux=aux->prox;
    }
    return 2;


}
void menu(){
	printf("\n--------------------MENU-----------------------\n");
	printf("[1]Cadastrar Show.\n");
	printf("[2]Remover Show.\n");
	printf("[3]Exibir Programacao.\n");
	printf("[0]Salvar e Sair...\n");
	printf("------------------------------------------------\n");
}


int main(){
    Dia dias[5];
    char artista[30];
    int dia,op,p;
    iniciarDias(dias);
    do{
        menu();
        scanf("%d",&op);
		system("cls");
        switch (op){
            case 1:
            	do{
	                printf("Dia:\n");
	                scanf("%d",&dia);
				}while(dia<1||dia>5);
                dia=dia-1;
                do{
                	printf("\nShow Principal:\n[0]Nao.\n[1]Sim.\n");
                	scanf("%d",&p);
				}while(op<0||op>1);
                cadastrarShow(dias,dia,p);
                break;
            case 2:
            	do{
	                printf("Dia:\n");
	                scanf("%d",&dia);
				}while(dia<1||dia>5);
                dia=dia-1;
                fflush(stdin);
                printf("\nArtista:\n");
                gets(artista);
                p=removerShow(dias,dia,artista);
                switch (p){
                	case 0:
						printf("\nRemovido com Sucesso!\n");
                		break;
                	case 1:
                		printf("\nNao foi possivel remover!\nOpcao cancelada\n");
                		break;
                	case 2:
						printf("\nNao foi possivel remover!\nArtista nao encontrado!\n");
                		break;
				}
				break;
            case 3:
                exibirProgramacao(dias);
                break;
        }
        system("Pause");
		system("cls");
    }while(op!=0);
	salvarProgramacao(dias);

    freeAll(dias);
    return 0;
}